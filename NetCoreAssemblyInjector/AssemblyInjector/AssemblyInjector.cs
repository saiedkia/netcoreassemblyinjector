﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;

namespace AssemblyInjector
{
    public static class AssemblyInjector
    {
        public static void RegisterAssembly<T>(this IServiceCollection serviceCollection)
        {
            Assembly assemblyType = typeof(T).Assembly;
            Type[] classes= assemblyType.GetTypes();

            foreach (Type t in classes)
            {
                Type[] interfaces = t.GetInterfaces();
                foreach(Type tInterface in interfaces)
                {
                    serviceCollection.AddSingleton(tInterface, t);
                }
            }
        }
    }
}
