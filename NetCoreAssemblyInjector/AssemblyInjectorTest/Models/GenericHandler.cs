﻿namespace AssemblyInjectorTest.Models
{
    public class GenericHandler : GenericHandlerInterface<ClassA>, GenericHandlerInterface<ClassB>
    {
        public void Handler(ClassB input)
        {
            // do nothing
        }

        public void Handler(ClassA input)
        {
            // do nothing
        }
    }
}
