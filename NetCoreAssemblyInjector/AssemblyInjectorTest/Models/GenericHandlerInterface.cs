﻿namespace AssemblyInjectorTest.Models
{
    public interface GenericHandlerInterface<T>
    {
        void Handler(T input);
    }
}
