﻿namespace AssemblyInjectorTest.Models
{
    public class InterfaceInjection
    {
        GenericHandlerInterface<ClassA> _handler;

        public InterfaceInjection(GenericHandlerInterface<ClassA> handler)
        {
            _handler = handler;
        }
    }
}
