using AssemblyInjector;
using AssemblyInjectorTest.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace AssemblyInjectorTest
{
    [TestClass]
    public class UnitTestInjection
    {
        [TestMethod]
        public void first_simple_test()
        {
            ServiceCollection collection = new ServiceCollection();
            collection.AddTransient(typeof(ClassA));
            IServiceProvider provider = collection.BuildServiceProvider();

            ClassA resultA = provider.GetService<ClassA>();
            ClassB resultB = provider.GetService<ClassB>();

            Assert.IsNotNull(resultA);
            Assert.IsNull(resultB);
        }

        [TestMethod]
        public void assembly_injectoin()
        {
            ServiceCollection collection = new ServiceCollection();
            collection.RegisterAssembly<GenericHandler>();
            IServiceProvider provider = collection.BuildServiceProvider();

            GenericHandler handlerA = (GenericHandler)provider.GetService<GenericHandlerInterface<ClassA>>();
            Assert.IsNotNull(handlerA);

            GenericHandler handlerB = (GenericHandler)provider.GetService<GenericHandlerInterface<ClassB>>();
            Assert.IsNotNull(handlerB);
        }



        [TestMethod]
        public void constructor_injection()
        {
            ServiceCollection collection = new ServiceCollection();
            collection.RegisterAssembly<GenericHandler>();
            collection.AddTransient<InterfaceInjection>();
            IServiceProvider provider = collection.BuildServiceProvider();


            InterfaceInjection i = provider.GetService<InterfaceInjection>();
            Assert.IsNotNull(i);
        }
    }
}
